#include "../Include/piston.hpp"



namespace SteamPiston{

    size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp) {
	    ((std::string*)userp)->append((char*)contents, size * nmemb);
	    return size * nmemb;
    }

    Piston::Piston(std::string api_key) : api_key(api_key){
        inform();
        curl = curl_easy_init();
        if(curl){
            set_curl_common();
        }else{
            std::cerr << "Failed to initialize curl!\n";
        }
        
    }

    info Piston::GetPlayerInfo(std::string steam_id){

        std::string url;
        url = pre_url + "ISteamUser/GetPlayerSummaries/v0002/?key=" + this->api_key + "&steamids=" + steam_id;   

        CURLcode resultCode;
	    std::string readBuffer;

        info temp;

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 

        resultCode = curl_easy_perform(curl);

        if(resultCode == 0){

            m_Json = json::parse(readBuffer);
            for (auto &mol : m_Json["response"]["players"][0].get<json::object_t>() ) {
                if(mol.second.is_string()){
                    temp.insert(std::pair<std::string, std::string>(mol.first, mol.second));
                }else if(mol.second.is_number()){
                    temp.insert(std::pair<std::string, std::string>(mol.first , std::to_string(mol.second.get<int>())));
                }else{
                    std::cerr << "Unsupported type \n";
                }
            }

            return temp;
        }else{
            std::cerr << "Failed to fetch steam api page!\n";
            std::cerr << "Result Code: " << resultCode << "\n";
            std::cerr << "Data: " << readBuffer << "\n";
        }

        temp.insert(std::pair<std::string, std::string >("empty","empty"));

        return temp;
    }

    info Piston::GetPlayerBans(std::string steam_id){
        std::string url;
        url = pre_url + "ISteamUser/GetPlayerBans/v1?key=" + this->api_key + "&steamids=" + steam_id;   

        CURLcode resultCode;
	    std::string readBuffer;

        info temp;

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer); 

        resultCode = curl_easy_perform(curl);

        if(resultCode == 0){
            
            m_Json = json::parse(readBuffer);
            for (auto &mol : m_Json["players"][0].get<json::object_t>() ) {
                if(mol.second.is_string()){
                    temp.insert(std::pair<std::string, std::string>(mol.first, mol.second));
                }else if(mol.second.is_number()){
                    temp.insert(std::pair<std::string, std::string>(mol.first , std::to_string(mol.second.get<int>())));
                }else if(mol.second.is_boolean()){
                    if(mol.second == true){
                        temp.insert(std::pair<std::string, std::string>(mol.first, "true"));
                    }else{
                        temp.insert(std::pair<std::string, std::string>(mol.first, "false"));
                    }
                }else{
                    std::cerr << "Unsupported type\n";
                }
            }

            return temp;
        }else{
            std::cerr << "Failed to fetch steam api page!\n";
            std::cerr << "Result Code: " << resultCode << "\n";
            std::cerr << "Data: " << readBuffer << "\n";
        }

        temp.insert(std::pair<std::string, std::string >("empty","empty"));

        return temp;
    }

    void Piston::inform(){
        std::cout << "Using Piston version: " << version << "\n";
        curl_version_info_data * vinfo = curl_version_info( CURLVERSION_NOW );
        if( vinfo->features & CURL_VERSION_SSL ){
            std::cout << "SSL Supported\n";
        }else{
            std::cerr << "NO SSL SUPPORT\n";
        }
    }

    void Piston::set_api_key(std::string api_key){
        this->api_key = api_key;
    }

    void Piston::set_curl_common(){
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true); // follow redirect
        curl_easy_setopt(curl, CURLOPT_HEADER, false);
    }

    Piston::~Piston(){
        curl_easy_cleanup(curl);
    }

}

