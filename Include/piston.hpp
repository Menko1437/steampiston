#if !defined(PISTON_HPP)
#define PISTON_HPP

/*
    Copyright (c) 2019, Menko1437
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// std
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

#include <curl/curl.h>

// json header only 
#include <nlohmann/json.hpp>

namespace SteamPiston{

    enum Visibility{
        Private = 1,
		FriendsOnly,
	    FriendsOfFriends,
		UsersOnly,
		Public
    };

    enum Status{
        Offline,
		Online,
		Busy,
		Away,
		Snooze,
		LookingToTrade,
	    LookingToPlay
    };

  
    using json = nlohmann::json;
    using info = std::unordered_map<std::string, std::string>;
    
    class Piston{
        public:
            explicit Piston(std::string api_key);
            ~Piston();

            void inform();
            void set_api_key(std::string api_key);
            void set_curl_common();

            // api methods
            info GetPlayerInfo(std::string steam_id);
            info GetPlayerBans(std::string steam_id);

        private:
            const std::string version = "0.3.0";

            std::string api_key;
            std::string pre_url = "https://api.steampowered.com/";
            
            CURL* curl;
            json m_Json;
    };
}

#endif // PISTON_HPP
