# SteamPiston

SteamPiston is C++ library used to get player information from steam

## Create piston object

```
SteamPiston::Piston piston("<your api key>");
```
## Get player info

Getting player info is made to be as close as possible to php api

First of all, create info datatype ( basicly an unordered_map )
Then asign it the GetPlayerInfo function.

```
SteamPiston::info pInfo;
pInfo = piston.GetPlayerInfo("<steam64 id>");
```

then you can simply access it like you would access map in C++
```
std::cout << "Player name: " << pInfo["personaname"] << "\n";
```

You can check player status
```
if(pInfo["personastate"] == std::to_string(SteamPiston::Status::Online)){
        std::cout << "Status: Online\n";
}
```

To see player ban status you can do similar things as above
```
SteamPiston::info pBans;
pBans = piston.GetPlayerBans("<steam64 id>");
std::cout << "Vac banned: " << pBans["VACBanned"] << "\n";
```


## Building

SteamPiston uses Meson Build System.
Curl is needed and Json ( https://github.com/nlohmann/json )
